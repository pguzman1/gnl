/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 10:11:56 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/16 10:12:03 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int		ft_or(char *path)
{
	int		fd;
	char	c;
	char	buf[5];
	char	buf2[30];
	int		ret;
	int		i;
	int		j = 0;
	int static		first = 0;
	fd = open(path, O_RDONLY);
	if (fd == -1)
		printf("nop");
	i = 0;
	while ((ret = read(fd, &c, 1)))
	{
		if (first == 0)
		{
			if (c != '\n')
			{
				buf[i++] = c;
			}
			else if (c == '\n')
			{
				buf[i] = '\0';
				first = 1;
				break ;
			}
			else if (c == EOF)
			{
				printf("EOOFF");
				first = 1;
				buf[i] = '\0';
			}
		}
		else if (first == 1)
		{
		if (c == EOF)
			buf2[j] = '\0';
		else
			buf2[j++] = c;
		}
	}

	printf("%s\n",buf);
	if (close(fd) == -1)
		printf("error");
	return (0);
}

int		main(int argc, char **argv)
{
	ft_open(argv[1]);
	ft_open(argv[1]);
	return (0);
}
