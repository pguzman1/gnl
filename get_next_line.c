/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 15:44:20 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/17 17:18:23 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include <stdio.h>

int		ft_strchr_i(char *s, char c)
{
	int		i;

	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			return (i);
		i++;
	}
	return (-1);
}


int		get_next_line(int const fd, char ** line)
{
	char	buf[BUFF_SIZE + 1];
	char static	*str = "";
	int		ret;
	
	if (str == NULL)
	{
		*line = NULL;
		return (0);
	}
	if (ft_strcmp(str, "") == 0)
	{
		while ((ret = read(fd, buf, BUFF_SIZE)))
		{
			buf[ret] = '\0';
			str = ft_strjoin(str, buf);
		}	
	}
	if (ft_strcmp(str, "\n") == 0)
	{
		*line = "";
		str = NULL;
		return (1);
	}
	else if (ft_strcmp(str, "") != 0 && ft_strchr(str, '\n'))
	{
		*line = ft_strsub(str, 0,ft_strchr_i(str, '\n'));
		str = ft_strdup(str + ft_strchr_i(str, '\n') + 1);
		str = (ft_strcmp(str, "") == 0) ? (str = NULL) : str;
		return (1);
	}
	return (0);
}

int		main(int argc, char **argv)
{
	char *line;
	int fd;
	int c;
	fd = open(argv[1], O_RDWR| O_APPEND | O_CREAT);
//	ft_putstr_fd("aaa\nbbb\nccc\nddd\n", fd);

	get_next_line(fd, &line);
	printf("line =%s////\n", line); //aaa
	get_next_line(fd, &line);
	printf("line =%s////\n", line); //bbb
	get_next_line(fd, &line);
	printf("line =%s////\n", line); //ccc
	get_next_line(fd, &line);
	printf("line =%s////\n", line); //ddd
	get_next_line(fd, &line);
	printf("line =%s////\n", line);//
	get_next_line(fd, &line);
	printf("line =%s////\n", line);
	get_next_line(fd, &line);
	printf("line =%s////\n", line);//
	get_next_line(fd, &line);
	printf("line =%s////\n", line);
}

