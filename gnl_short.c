/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 15:44:20 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/17 20:04:36 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include <stdio.h>

int		ft_strchr_i(char *s, char c)
{
	int		i;

	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			return (i);
		i++;
	}
	return (-1);
}



int		get_next_line(int const fd, char ** line)
{
	char	buf[BUFF_SIZE + 1];
	char static	*str = "";
	int		ret;

	/*	if (str == NULL)
		{
		free(*line);
	 *line = NULL;
	 return (0);
	 }*/
			while ((ret = read(fd, buf, BUFF_SIZE)))
			{
				buf[ret] = '\0';
				str = ft_strjoin(str, buf);	
			}	
	/*	if (ft_strcmp(str, "\n") == 0)
		{
		free(*line);
	 *line = "";
	 free(str);
	 str = NULL;
	 return (1);
	 }*/
	//	if (/*ft_strcmp(str, "") != 0 && */str == NULL || ft_strchr(str, '\n'))
	//	{
	*line = (!str) ? (NULL) : ft_strsub(str, 0,ft_strchr_i(str, '\n'));
	str = (!str) ? (NULL) : ft_strdup(str + ft_strchr_i(str, '\n') + 1);
	if (str != NULL)
		str = (ft_strcmp(str, "") == 0) ? (str = NULL) : str;
	return (*line == NULL || str == NULL) ? (0) : (1);
}

int		main(int argc, char **argv)
{
	char *line;
	int fd;
	int c;
	//fd = open(argv[1], O_RDWR| O_APPEND | O_CREAT);
	
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line); //aaa
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line); //bbb
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line); //ccc
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line); //ddd
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line);//
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line);

	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line);//
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line);
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line);
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line);//
	printf("gnl_ret =%d      ",get_next_line(fd, &line));
	printf("line =%s////\n", line);

}

//what's the value of *line after i read all lines?
//what's the return if my *line is NULL
//
//
//i don't understand the 5th test (moulitest)
//
